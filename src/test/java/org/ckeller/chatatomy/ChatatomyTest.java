package org.ckeller.chatatomy;

import jersey.repackaged.com.google.common.collect.ImmutableMap;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import java.util.Map;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;

/**
 * Unit tests for Chatatomy with known inputs/outputs using Jersey's Grizzly
 * servlet container testing suite.
 *
 * Ideally we wouldn't depend on external resources (i.e. links out of our control)
 * for pass/fail since they most definitely are intermittent, but I wanted to test
 * with the given input for this exercise.
 *
 * @author ckeller
 */
public class ChatatomyTest extends JerseyTest {

    Map<String, String> testData = ImmutableMap.of(
            "@chris you around?",
                    "{\n" +
                    "  \"mentions\": [\n" +
                    "    \"chris\"\n" +
                    "  ]\n" +
                    "}",
            "Good morning! (megusta) (coffee)",
                    "{\n" +
                    "  \"emoticons\": [\n" +
                    "    \"megusta\",\n" +
                    "    \"coffee\"\n" +
                    "  ]\n" +
                    "}",
            "Olympics are starting soon; http://www.nbcolympics.com",
                    "{\n" +
                    "  \"links\": [\n" +
                    "    {\n" +
                    "      \"url\": \"http://www.nbcolympics.com\",\n" +
                    "      \"title\": \"2016 Rio Olympic Games | NBC Olympics\"\n" +
                    "    }\n" +
                    "  ]\n" +
                    "}",
            "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016",
                    "{\n" +
                    "  \"mentions\": [\n" +
                    "    \"bob\",\n" +
                    "    \"john\"\n" +
                    "  ],\n" +
                    "  \"emoticons\": [\n" +
                    "    \"success\"\n" +
                    "  ],\n" +
                    "  \"links\": [\n" +
                    "    {\n" +
                    "      \"url\": \"https://twitter.com/jdorfman/status/430511497475670016\",\n" +
                    "      \"title\": \"Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;\"\n" +
                    "    }\n" +
                    "  ]\n" +
                    "}"
            );

    @Override
    protected Application configure() {
        return new ApplicationConfig();
    }

    @Test
    public void given() {
        testData.forEach((input,expected)->{
            String actual = target("1.0").request().post(Entity.text(input), String.class);
            assertJsonEquals(expected, actual);
        });
    }

    @Test
    public void multipleLinks() {
        String input = "@bob, @thomas check these (sweet) links out: http://www.afghan-web.com/economy/, " +
                "http://www.libregraphicsmeeting.org/2016/attend/getting-to-the-campus/, " +
                "https://www.w3.org/Protocols/HTTP/1.1/rfc2616.pdf, http://216.58.194.142";
        String expected = "{\n" +
                "  \"mentions\": [\n" +
                "    \"bob\",\n" +
                "    \"thomas\"\n" +
                "  ],\n" +
                "  \"emoticons\": [\n" +
                "    \"sweet\"\n" +
                "  ],\n" +
                "  \"links\": [\n" +
                "    {\n" +
                "      \"url\": \"http:\\/\\/www.afghan-web.com\\/economy\\/\",\n" +
                "      \"title\": \"Afghanistan Online: Economy\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"url\": \"http:\\/\\/www.libregraphicsmeeting.org\\/2016\\/attend\\/getting-to-the-campus\\/\",\n" +
                "      \"title\": \"Getting to the campus &#8211; Libre Graphics Meeting 2016\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"url\": \"https:\\/\\/www.w3.org\\/Protocols\\/HTTP\\/1.1\\/rfc2616.pdf\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"url\": \"http:\\/\\/216.58.194.142\",\n" +
                "      \"title\": \"Google\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        String actual = target("1.0").request().post(Entity.text(input), String.class);
        assertJsonEquals(expected, actual);
    }
}