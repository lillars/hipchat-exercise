package org.ckeller.chatatomy.resource.bean;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Parsed data bean
 *
 * @author ckeller
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ParsedData {
    private String[] mentions;
    private String[] emoticons;
    private Link[] links;

    public String[] getMentions() {
        return mentions;
    }

    public void setMentions(String[] mentions) {
        this.mentions = mentions;
    }

    public String[] getEmoticons() {
        return emoticons;
    }

    public void setEmoticons(String[] emoticons) {
        this.emoticons = emoticons;
    }

    public Link[] getLinks() {
        return links;
    }

    public void setLinks(Link[] links) {
        this.links = links;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Link {
        private String url;
        private String title;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
