package org.ckeller.chatatomy.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.ckeller.chatatomy.resource.bean.ParsedData;
import org.ckeller.chatatomy.resource.bean.ParsedData.Link;
import org.glassfish.jersey.client.ClientProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Web resource to parse a chat message for special tags. This is
 * a singleton for performance reasons since it utilizes a JAX-RS client
 * which is expensive to instantiate. It's safe to do so since this class is thread-safe
 * itself and essentially stateless as it only contains the client which is also
 * thread-safe and encouraged to be shared.
 *
 * @author ckeller
 */
@Singleton
@Path("1.0") // ver. 1.0
public class ParserResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParserResource.class);

    // Regex constants
    private static final Pattern MENTIONS_PATTERN = Pattern.compile("@(\\w+)");
    private static final Pattern EMOTICON_PATTERN = Pattern.compile("\\((\\w{1,15})\\)");
    private static final Pattern URL_PATTERN = Pattern.compile(
            "(https?://[-A-Za-z0-9+&@#/%?=()~_|!:,.;]+[-A-Za-z0-9+&@#/%=()~_|])");
    private static final Pattern TITLE_TAG_PATTERN =
            Pattern.compile("<title>(.*)</title>", Pattern.CASE_INSENSITIVE|Pattern.DOTALL);

    // The JAX-RS client. This is thread-safe.
    private final Client client;

    public ParserResource() {
        // Set a timeout on the client for a few seconds. I'm assuming that
        // the title information can be treated as extraneous as a trade off for waiting too long.
        client = ClientBuilder.newClient().
                property(ClientProperties.CONNECT_TIMEOUT, 5000).
                property(ClientProperties.READ_TIMEOUT, 5000);
    }

    /**
     * POST method to parse a chat message in the body to find "mentions" tags, emoticons,
     * and URL HTML links. For the links, a JAX-RS client request will be kicked
     * off to discover the page title.
     *
     * @param message message to parse
     * @return a web response containing the aforementioned special tags as JSON. Any
     * missing tags will not be present.
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public ParsedData parseMessage(String message) {
        LOGGER.debug("Message received: {}", message);

        StringBuilder extractedMessage = new StringBuilder(message);
        ParsedData parsedData = new ParsedData();

        // Extract the urls first since they can have the same characters as the mentions
        // and emoticons which can cause false positives
        String[] urls = extractMatches(URL_PATTERN, extractedMessage);
        if (urls != null) {
            Link[] links = new Link[urls.length];
            ExecutorService executor = Executors.newCachedThreadPool();
            List<Callable<Object>> titleQueries = new ArrayList<>();
            for (int i = 0; i < links.length; i++) {
                final String url = urls[i];
                final Link link = links[i] = new Link();
                links[i].setUrl(url);

                // Use a thread-pool for HTTP GET queries for the HTML titles for
                // added efficiency with messages containing multiple links
                titleQueries.add(Executors.callable(() -> {
                    try {
                        String title = fetchTitle(url);
                        LOGGER.debug("URL: {}, Title: {}", url, title);
                        link.setTitle(title);
                    } catch (Exception e) {
                        // Don't fail just because we can't get the title. Leave it
                        // as null and continue on. The client should handle this
                        // like other missing fields.
                        LOGGER.error("Unable to fetch title from url: {}", url, e);
                    }
                }));
            }

            try {
                // Wait for all title query threads to complete
                executor.invokeAll(titleQueries);
            } catch (InterruptedException e) {
                // Will never happen so we can safely ignore this
            }
            parsedData.setLinks(links);
        }
        parsedData.setMentions(extractMatches(MENTIONS_PATTERN, extractedMessage));
        parsedData.setEmoticons(extractMatches(EMOTICON_PATTERN, extractedMessage));

        if (LOGGER.isDebugEnabled()) {
            ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
            try {
                LOGGER.debug("JSON response:\n{}", objectWriter.writeValueAsString(parsedData));
            } catch (JsonProcessingException e) {
                LOGGER.debug("Unable to log parsed data as JSON", e);
            }
        }

        return parsedData;
    }

    /**
     * Utilizes the JAX-RS client to parse the title from a given link
     *
     * @param url the url to call
     * @return the HTML title or null if it doesn't exist
     * @throws Exception
     */
    private String fetchTitle(String url) throws Exception {
        Response response = client.target(url).request().get();
        MediaType mediaType = response.getMediaType();

        // Only parse HTML
        if (mediaType.isCompatible(MediaType.TEXT_HTML_TYPE)) {

            // Handle appropriate character encoding; use system default if one is not present
            String charset = mediaType.getParameters().get(MediaType.CHARSET_PARAMETER) == null ?
                    Charset.defaultCharset().name() : mediaType.getParameters().get(MediaType.CHARSET_PARAMETER);
            try (
                    InputStream inputStream = response.readEntity(InputStream.class);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, charset));
                    Scanner scanner = new Scanner(reader)
            ) {
                // Use Scanner to parse as little of the HTML stream as possible to find the title
                // An alternative would be to use an HTML parsing library like jsoup. While that would likely
                // be more accurate than a regex approach, it's also heavy-handed since it has to read and parse
                // the entire response into a DOM object just to get a single field. I considered speed and memory
                // paramount here.
                if (scanner.findWithinHorizon(TITLE_TAG_PATTERN, 0) != null) {
                    return scanner.match().group(1);
                }
            }
        }
        return null;
    }

    /**
     * Extracts and returns matches from a given StringBuilder.
     *
     * @param pattern regex to match
     * @param input StringBuilder to extract from (contents will be modified on match)
     * @return an array of a given regex pattern matches; null if no matches found.
     */
    private String[] extractMatches(Pattern pattern,
                                    StringBuilder input) {
        Matcher matcher = pattern.matcher(input);
        List<String> matches = new ArrayList<>();
        while(matcher.find()) {
            matches.add(matcher.group(1));
            input.delete(matcher.start(), matcher.end());
            matcher.reset();
        }
        return matches.isEmpty() ? null : matches.toArray(new String[matches.size()]);
    }

    /**
     * Close the JAX-RS client upon destruction
     */
    @PreDestroy
    private void cleanup() {
        client.close();
    }
}