package org.ckeller.chatatomy;

import org.ckeller.chatatomy.resource.ParserResource;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

/**
 * Servlet 3.x annotation based application definition
 *
 * @author ckeller
 */
@ApplicationPath("chatatomy")
public class ApplicationConfig extends ResourceConfig {
    public ApplicationConfig() {
        register(ParserResource.class);
    }
}