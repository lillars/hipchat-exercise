#Chatatomy: a HipChat exercise

A portmanteau of chat and anatomy; this is a RESTful API servlet that finds all @mentions, (emoticons), and
links inside a chat message and returns them in JSON format for further processing by a chat client (e.g. HipChat).
To use it, simply deploy it to your favorite servlet 3.0+ container.

```
Input: "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"
Return:
{
  "mentions": [
    "bob",
    "john"
  ],
  "emoticons": [
    "success"
  ],
  "links": [
    {
      "url": "https://twitter.com/jdorfman/status/430511497475670016",
      "title": "Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"
    }
  ]
}
```

####Parsed keywords: 

1. **mentions** - A way to mention a user. Always starts with an '@' and ends when hitting a non-word character.
2. **emoticons** - Consider 'custom' emoticons which are alphanumeric strings no longer than 15 characters and contained in parenthesis.
3. **links** - Any URLs contained in the message along with the page's title.

####Dependencies:

1. Jersey 2.x for JAX-RS servlet, JSON, and client functionality
2. SLF4J for logging
3. json-unit for easy JSON unit testing
